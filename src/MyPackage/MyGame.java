package MyPackage;

//JFC
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Dimension;

//GTGE
import com.golden.gamedev.Game;
import com.golden.gamedev.GameLoader;
import com.golden.gamedev.object.*;
import com.golden.gamedev.object.background.ImageBackground;
import java.util.ArrayList;
import java.util.Random;
import java.util.TimerTask;
import java.util.Timer;

import javax.swing.JOptionPane;

public class MyGame extends Game implements EntityEventListner {
    private Color backgroundColor = Color.BLACK;
    
    private SpriteGroup	nutritiousParticles; // Питательные частицы
    public ArrayList <BaseElement> getNutritiousParticles(){
        ArrayList <BaseElement> list = new ArrayList<>();
        for (Sprite np : nutritiousParticles.getSprites()){
            if (np!=null && np.isActive())
                list.add((BaseElement)np);
        }
        return list;
    }
    private SpriteGroup	entitys; // Сущности, которые могу поедать
    public ArrayList <BaseElement> getEntitys(){
        ArrayList <BaseElement> list = new ArrayList<>();
        for (Sprite np : entitys.getSprites()){
            if (np!=null && np.isActive())
                list.add((BaseElement)np);
        }
        return list;
    }
    
    private CollisionMaster collisionMaster;
    private Background backgr;
    public Background getBackground(){
        return backgr;
    }
    
    private Player player;
    private Random random = new Random();
    private ArrayList <PlayerBot> bots;
    
    static public final int bw = 2560;   // background width
    static public final int bh = 1440;   // background height

    
    public static void main(String[] args) {
        GameLoader game = new GameLoader();
        game.setup(new MyGame(), new Dimension(640,480), false);
        game.start();
    }
    
    @Override
    public void initResources() {
        backgr = new ImageBackground(getImage("src/space.jpg"), bw, bh);
        collisionMaster = new CollisionMaster();
        // initialization of game variables
    	int h = getHeight(), w = getWidth();
        entitys = new SpriteGroup("Entitys");
        nutritiousParticles = new SpriteGroup("Nutritious Particles");
        
        //Создаем игрока
    	Bacterium bacterium = new Bacterium(SpecializationFactory.create(Specializations.PRIMARY_BACTERIUM));
    	bacterium.setPosition(bw*.5, bh*.5);
        entitys.add(bacterium);
        player = new Player(bacterium, this);
        bacterium.setPlayer(player);
        bacterium.addListner(this);
        //генератор питательных частиц
        final Timer timerNutr = new Timer();
        timerNutr.schedule(new TimerTask(){
            NutritiousParticles [] types = NutritiousParticles.values();
            @Override
            public void run() {
                float x,y,cnt = types.length;
                for(int i = 0; i < cnt; i++)
                {
                    x = random.nextFloat();
                    y = random.nextFloat();
                    x = x - (int)x;
                    y = y - (int)y;
                    x = x > 0 ? bw*x : -bw*x;
                    y = y > 0 ? bh*y : -bh*y;
                    NutritiousParticle np = NutritiousParticleFactory.create(types[i]);
                    int R = np.R;
                    
                    if(x < R) x = R;
                    if(y < R) y = R;
                    if( bw-x < R ) x = bw-R;
                    if( bh-y < R ) y = bh-R;
                    np.setPosition(x, y);
                    nutritiousParticles.add(np);
                }
            }
        },1000,1000);
        
        //генератор игроков - ботов
        MyGame mygame = this;
        bots = new ArrayList<>();
        final Timer timerBot = new Timer();
        timerBot.schedule(new TimerTask(){
            Bacterium newBacterium = null;
            PlayerBot mewBot = null;
            @Override
            public void run() {
                float x,y;
                x = random.nextFloat();
                y = random.nextFloat();
                x = x - (int)x;
                y = y - (int)y;
                x = x > 0 ? bw*x : -bw*x;
                y = y > 0 ? bh*y : -bh*y;
                Specialization spec = SpecializationFactory.create(Specializations.PRIMARY_BACTERIUM);
                newBacterium = new Bacterium(spec);
                mewBot = new PlayerBot(newBacterium, mygame);
                entitys.add(newBacterium);
                newBacterium.setPlayer(mewBot);
                
                int R = newBacterium.R;
                if(x < R) x = R;
                if(y < R) y = R;
                if( bw-x < R ) x = bw-R;
                if( bh-y < R ) y = bh-R;
                
                newBacterium.setPosition(x, y);
                newBacterium.addListner(mygame);
                bots.add(mewBot);
            }
        },5000,5000);
        
        collisionMaster.addCollision(entitys, nutritiousParticles);
        collisionMaster.addCollision(entitys, entitys);
    }
    
    @Override
    public void update(long elapsedTime) {
        if(!player.bacterium.isActive())
        {
            JOptionPane.showMessageDialog(null, "Вы проиграли :(");
            System.exit(0);
        }
                
        player.move();
        ArrayList<PlayerBot> notActiveBots = new ArrayList<>();
        for(PlayerBot bot : bots){
            if(!bot.bacterium.isActive())
                notActiveBots.add(bot);
        }
        bots.removeAll(notActiveBots);
        try{
            for(PlayerBot bot : bots){
                bot.move();
        }
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        ArrayList<BaseElement> nutritiousParticles1 = getNutritiousParticles();
        for(BaseElement nut : nutritiousParticles1){
            nut.continueMovement();
        }

    	backgr.update(elapsedTime);
        
    	entitys.update(elapsedTime);
        nutritiousParticles.update(elapsedTime);
        
        entitys.setBackground(backgr);
        nutritiousParticles.setBackground(backgr);
        
        try {
            collisionMaster.checkCollisions();
        } catch (Exception e) {
        }
    }
    
    @Override
    public void render(Graphics2D g) {
        // rendering to the screen
    	g.setColor(backgroundColor);
    	g.fillRect(0, 0, getWidth(), getHeight());
    	backgr.render(g);
        nutritiousParticles.render(g);
        entitys.render(g);
    }
    
    public Specializations offerChangeSpec(Player player, ArrayList<Specializations> specList)
    {
        int cnt = specList.size();
        String[] list = new String [cnt];
        for (int i = 0; i < cnt; i++)
        {
            list[i] = specList.get(i).getName();
        }
        Object s = JOptionPane.showInputDialog(null,"","Эволюционировать в:", JOptionPane.QUESTION_MESSAGE,null, list, list[0]);
        if (s != null)
        {
            for(int i = 0; i < cnt;i++)
                if(s.equals(list[i])) return specList.get(i);
        }
        return null;    
    }

    @Override
    public void actionToEntityEvent(EntityEvent event) {
        //добавляем результат жизнедеятельности на поле
        NutritiousParticle result = event.getResult();
        Entity entity = event.getEntity();
        double energyX = entity.getEnergyX();
        double energyY = entity.getEnergyY();
        int R = entity.getR();
        int kx = (int)(energyX/energyX);
        int ky = (int)(energyY/energyY);
        result.setPosition(entity.getX()+ R*kx, entity.getY() + R*ky);
        result.moveTo(-entity.getEnergyX(), -entity.getEnergyY());
        nutritiousParticles.add(result);
    }
}