/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

import java.util.ArrayList;

/**
 *
 * @author Elena Sarkisova
 */
class Recipe
{
    class RecipeElement
    {
        Specializations spec = null;
        NutritiousParticle particle = null;
        double maxWeightScale = 0; //соотношение масс
        RecipeElement(Specializations spec,double maxWeightScale)
        {
            this.spec = spec;
            this.maxWeightScale = maxWeightScale;
        }
        RecipeElement(NutritiousParticle agar)
        {
            this.particle = agar;
        }
    }
    ArrayList<RecipeElement> recipe = new ArrayList <RecipeElement>();
    double efficiency = 1;
    NutritiousParticles result = null;
    
    public boolean check(BaseElement element, int weight)
    {
        for(RecipeElement re : recipe)
        {
            if (element instanceof NutritiousParticle)
            {
                if(re.particle != null && element.getClass() == re.particle.getClass())
                    return true;
            }
            else
            {
                if(re.particle == null && re.spec == ((Entity)element).specialization.spec && re.maxWeightScale > element.weight/weight)
                {
                    return true;
                }
            }
        }
        return false;
    }
    public void setEfficiency(double val)
    {
        efficiency = val;
    }
    public void setResult(NutritiousParticles val)
    {
        result = val;
    }
    public void add(Specializations spec,double maxWeightScale)
    {
        recipe.add(new RecipeElement(spec,maxWeightScale));
    }
    public void add(NutritiousParticle agar)
    {
        recipe.add(new RecipeElement(agar));
    }
    
    public boolean isPosible(ArrayList <BaseElement> food, ArrayList<BaseElement> col){
        for (RecipeElement re : recipe){
            boolean ok = false;
            for (BaseElement be : food){
                if (be instanceof NutritiousParticle)
                {
                    if(re.particle != null && be.getClass() == re.particle.getClass()){
                        ok=true;
                        col.add(be);
                        break;
                    }
                }
                else
                {
                    if(re.particle == null && re.spec == ((Entity)be).specialization.spec)
                    {
                        ok=true;
                        col.add(be);
                        break;
                    }
                }
            }
            if(!ok){
                col.clear();
                return false;
            }  
        }
        return true;
    }
}