/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

import java.util.ArrayList;

/**
 *
 * @author Elena Sarkisova
 */
public abstract class Entity extends BaseElement{
    protected Specialization specialization;
    ArrayList <BaseElement> food = new ArrayList<BaseElement>();
    private NutritiousParticle result = null;//результат жизнедеятельности
    protected int maxWeight;
    private ArrayList<EntityEventListner> listeners = new ArrayList<EntityEventListner>();
    public int getMaxWeight()
    {
        return maxWeight;
    }
    public NutritiousParticle getResult()
    {
        return result;
    }
    public void resetResult()
    {
        result = null;
    }
    
    public void addListner(EntityEventListner eel)
    {
        listeners.add(eel);
    }
    private Player player;
    public Entity(int weight,Specialization s) {
        super(weight, s.name, s.color);
        specialization = s;
        addListner(new EvolutionTree());
        maxWeight = weight;
    }
    public void setPlayer(Player P)
    {
        player = P;
    }
    public Player getPlayer()
    {
        return player;
    }
    public Specialization getSpecialization()
    {
        return specialization;
    }
    public void  SetSpecialization(Specialization s )
    {
        this.specialization = s;
        this.color = s.color;
        this.name = s.name;
        this.setImage(createCircle());
    }
    //увеличить массу
    public void plumpUp(int delta)
    {
        weight += delta;
        maxWeight = weight > maxWeight ? weight : maxWeight;
        R = (int)(Math.sqrt(weight)*10);
        this.setImage(createCircle());
    }
    /**
     * Поглотить
    */
    public boolean eat(BaseElement f)
    {
        if(specialization.ration.check(f, weight))
        {
            food.add(f);
            digest();
            return true;
        }
        return false;
    }
    /**
     * Переварить
     */
    private void digest()
    {
        ArrayList<BaseElement> col = new ArrayList<BaseElement>();
        Recipe r = specialization.ration.getPosibleRecipe(food, col);
        if (r == null)
            return;
        int sumWeight = 0;
        for(BaseElement be : col)
        {
            sumWeight += be.weight;
        }
        sumWeight *= r.efficiency;
        plumpUp(sumWeight);
        food.removeAll(col);
        fireEntityEvent(this,sumWeight,NutritiousParticleFactory.create(r.result));
    }

    protected void fireEntityEvent(Entity entity, int weightDelta, NutritiousParticle res){
       EntityEvent event = new EntityEvent(entity,weightDelta,res);
        for (EntityEventListner listener : listeners){
            listener.actionToEntityEvent(event);
        }
    }
}
