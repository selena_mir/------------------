/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

/**
 *
 * @author Elena Sarkisova
 */
public enum Specializations {
    SIMPLEST_ANIMAL("простейшее животное"), //простейшее животное
    SIMPLEST_PLANT("простейшее растение"),  //простейшее растение
    PRIMARY_BACTERIUM("первичная бактерия"),    //первичная бактерия
    MOSS_BACTERIUM("бактерия-мох"),     //бактерия-мох
    PLANT_PARASITE("растение-парзит"),  //растение-парзит
    PLANT_PREDATOR("растение-хищник"),  //растение-хищник
    HERBIVOROUS_ANIMAL("растениядное животное"),    //растениядное животное
    CARNIVOROUS_ANIMAL("хищное животное"),  //хищное животное
    BUFFALO_BACTERIUM("бактерия-буйвол"),   //бактерия-буйвол
    OMNIVORE_ANIMAL("всеядное животное"),   //всеядное животное
    TIGER_BACTERIUM("бактерия-тигр");       //бактерия-тигр
    
    Specializations(String name){
        this.name=name;
    }
    String name;
    public String getName() {
        return name;
    }
}