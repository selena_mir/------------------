package MyPackage;

import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.SpriteGroup;
import com.golden.gamedev.object.collision.BasicCollisionGroup;

public class EnemyCollision extends BasicCollisionGroup {

    public EnemyCollision(SpriteGroup group1, SpriteGroup group2) {
           // register the collision between what group to what group
           setCollisionGroup(group1, group2);

       }
    @Override
    public void collided(Sprite s1, Sprite s2) {
        // TODO Auto-generated method stub
        BaseElement c1 = (BaseElement) s1, c2 = (BaseElement) s2;
        NutritiousParticle result = null;
        double d = Math.pow(c1.centerX() - c2.centerX(),2) + Math.pow(c1.centerY() - c2.centerY(),2);
        double maxR = Math.max(c1.getR(),c2.getR());
        if (maxR*maxR>d)
        {
            boolean f =false;
            if(s1 instanceof Entity)
            {
                if(((Entity)s1).eat(c2)){
                    c2.setActive(false);
                    f=true;
                }
            }
            if (!f && s2 instanceof Entity){
                if(((Entity)s2).eat(c1))
                    c1.setActive(false);
            }
        }
    }

}
