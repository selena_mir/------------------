/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

/**
 *
 * @author Elena Sarkisova
 */
public class Bacterium extends Entity{

    public Bacterium(int weight, Specialization s) {
        super(weight, s);
    }
    
    public Bacterium(Specialization s) {
        super(3, s);
    }
    
}
