/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Elena Sarkisova
 */
public class EvolutionTree implements EntityEventListner{
    private static HashMap <Specializations, ArrayList <Specializations> > tree = new HashMap< >();
    private static double weightLimits [] = new double[] {10.0, 20.0, 30.0, 40.0};
    EvolutionTree()
    {
        ArrayList <Specializations> prBact = new ArrayList <>();
        prBact.add(Specializations.SIMPLEST_ANIMAL);
        prBact.add(Specializations.SIMPLEST_PLANT);
        tree.put(Specializations.PRIMARY_BACTERIUM,prBact);
        
        ArrayList <Specializations> smpPl = new ArrayList <>();
        smpPl.add(Specializations.MOSS_BACTERIUM);
        smpPl.add(Specializations.PLANT_PARASITE);
        smpPl.add(Specializations.PLANT_PREDATOR);
        tree.put(Specializations.SIMPLEST_PLANT,smpPl);
        
        ArrayList <Specializations> smpAn = new ArrayList <>();
        smpAn.add(Specializations.HERBIVOROUS_ANIMAL);
        smpAn.add(Specializations.CARNIVOROUS_ANIMAL);
        smpAn.add(Specializations.OMNIVORE_ANIMAL);
        tree.put(Specializations.SIMPLEST_ANIMAL,smpAn);
        
        ArrayList <Specializations> herbAn = new ArrayList <>();
        herbAn.add(Specializations.BUFFALO_BACTERIUM);
        tree.put(Specializations.HERBIVOROUS_ANIMAL,herbAn);
        
        ArrayList <Specializations> carnAn = new ArrayList <>();
        carnAn.add(Specializations.TIGER_BACTERIUM);
        tree.put(Specializations.CARNIVOROUS_ANIMAL,carnAn);
    }
    /*
    Получить список возможных специализаций
    */
    private static ArrayList<Specializations> getSpecializationsToEvolution(Specializations spec)
    {
        return tree.get(spec);
    }
    /**
     * Получить список всех специализаций - потомков заданной
    */
    public static ArrayList<Specializations> getAllDescendans(Specializations ancestor)
    {
        ArrayList<Specializations> result = tree.get(ancestor);
        if(result == null) return null;
        int cnt = result.size();
        for (int i  = 0; i < cnt; i++)
        {
            ArrayList<Specializations> tmp = getAllDescendans(result.get(i));
            if (tmp != null)
            result.addAll(tmp);
        }
        return result;
    }
    /**
     * Эволюция
    */
    @Override
    public void actionToEntityEvent(EntityEvent event) {
        Entity entity = event.getEntity();
        int weightDelta = event.getWeightDelta();
        int maxWeight = entity.getMaxWeight();
        int prevWeight = entity.getMaxWeight() - weightDelta;
        for(int i = 0 ; i < weightLimits.length;i++)
        {
            if( weightLimits[i] <= maxWeight && weightLimits[i] > prevWeight )
            {
                ArrayList<Specializations> specializationsToEvolution = getSpecializationsToEvolution(entity.getSpecialization().spec);
                if(specializationsToEvolution !=  null)
                {
                    //предложить пользователю эволюционировать
                    System.out.print(specializationsToEvolution);
                    entity.getPlayer().chooseSpec(specializationsToEvolution);
                }
            }
        }
    }
}
