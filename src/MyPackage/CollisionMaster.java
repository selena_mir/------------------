/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

import com.golden.gamedev.object.SpriteGroup;
import java.util.ArrayList;

/**
 *
 * @author MegoComp
 */
public class CollisionMaster {
    ArrayList <EnemyCollision> collisions;

    public CollisionMaster() {
        collisions = new ArrayList<>();
    }
    
    public void addCollision(SpriteGroup group1, SpriteGroup group2){
        collisions.add(new EnemyCollision(group1, group2));
    }
    
    public void checkCollisions(){
        for (EnemyCollision ec : collisions){
            ec.checkCollision();
        }
    }
}
