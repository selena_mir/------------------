/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

/**
 *
 * @author Elena Sarkisova
 */
public class NutritiousParticleFactory {
    public static NutritiousParticle create(NutritiousParticles type)
    {
        switch (type)
        {
            case AGAR:
                return new Agar();
            case LIGHT:
                return new Light();
            case WATER:
                return new Water();
            case CO2:
                return new CO2();
            case OXYGEN:
                return new Oxygen();
        }
        return null;
    }
}
