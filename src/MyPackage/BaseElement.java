package MyPackage;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.golden.gamedev.object.Sprite;

public abstract class BaseElement extends Sprite {
    private double energyY = 0; //Энергия
    private double energyX = 0; //Энергия
    private double angl = 0;    //Угол движения
    protected int weight;       //Масса
    private int maxEnergy = 1000;
    private int delta = 40;
    protected Color color;
    protected String name;

    int R;
    private Color fontColor = Color.BLACK;
    public int getWeight()
    {
       return weight;
    }
    public BaseElement (int weight, String name,Color col)
    {
        this.weight = weight;
        this.R = (int)(Math.sqrt(weight)*10);
        this.color = col;
        this.name = name;
        this.setImage(createCircle());
    }
    public void setPosition(double x,double y)
    {
            this.setX(x-R);
            this.setY(y-R);
    }
    public double centerX()
    {
            return this.getX()+ R;
    }
    public double centerY()
    {
            return this.getY()+ R;
    }
    public int getR()
    {
            return R;
    }
    protected BufferedImage createCircle()
    {
        BufferedImage img = new BufferedImage(2*R, 2*R,BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D)img.getGraphics();
        g.setColor(color);
        g.fillOval(0, 0, 2*R, 2*R);
        g.setColor(fontColor);
        FontMetrics fm = g.getFontMetrics();
        int sw = fm.stringWidth(name);
        Font f = g.getFont();
        int hs = f.getSize();
        g.drawString(name, R-sw/2, R+hs/3);
        return img;
    }
    public double getEnergyX()
    {
        return energyX;
    }
    public double getEnergyY()
    {
        return energyY;
    }
    public void moveTo(int mx, int my){
        moveTo(mx, my, maxEnergy);
    }
    
    public void moveTo(double energyX, double energyY)
    {
        this.energyX = energyX;
        this.energyY = energyY;
    }
    
    public void moveTo(int mx, int my, double energy)
    {
        double sx = centerX(),sy = centerY();
        energyX = (mx-sx)*(mx-sx)/((mx-sx)*(mx-sx) + (my-sy)*(my-sy)) * energy * (mx-sx)/Math.abs((mx-sx));
        energyY = (my-sy)*(my-sy)/((mx-sx)*(mx-sx) + (my-sy)*(my-sy)) * energy * (my-sy)/Math.abs((my-sy));
    }
    
    public void continueMovement(){
        setSpeed(Math.sqrt(Math.abs(energyX/width))* (energyX/width<0 ? -1 : 1)/20, Math.sqrt(Math.abs(energyY/width))* (energyY/width<0 ? -1 : 1)/20);
        //Энергия по Х
        if (Math.abs(energyX)<delta)
            energyX=0;
        else
            energyX=(energyX < 0 ? -1 : 1) * (Math.abs(energyX) - delta);

        //Энергия по Y
        if (Math.abs(energyY)<delta)
            energyY=0;
        else
            energyY=(energyY < 0 ? -1 : 1) * (Math.abs(energyY) - delta);
        
        if (centerX()<R){
            energyX=0;
            setLocation(0, getY());
        }
        if (centerY()<R){
            energyY=0;
            setLocation(getX(), 0);
        }
        if (centerY() > MyGame.bh - R){
            energyY=0;
            setLocation(getX(), MyGame.bh - 2*R);
        }
        if (centerX() > MyGame.bw - R){
            energyX=0;
            setLocation(MyGame.bw - 2*R, getY());
        }
    }
}
