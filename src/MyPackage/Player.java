/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author MegoComp
 */
public class Player {
    Bacterium bacterium;
    MyGame game;
    public Player(Bacterium bacterium, MyGame game) {
        this.bacterium = bacterium;
        this.game = game;
    }
    
    protected void moveTo (int x, int y){
        if (!pointInArea(new Point(x, y), bacterium.R)){
            bacterium.moveTo(x, y);
        }
        bacterium.continueMovement();
    }
    
    public void move(){
        moveTo((int)(game.getMouseX()+game.getBackground().getX()), (int)(game.getMouseY()+game.getBackground().getY()));
        game.getBackground().setToCenter(bacterium);
    }
    
    protected boolean pointInArea(Point p, double r){
        double cx = bacterium.centerX();
        double cy = bacterium.centerY();
        return Math.sqrt((cx-p.x)*(cx-p.x) + (cy-p.y)*(cy-p.y)) <=r;
    }
    
    public void chooseSpec(ArrayList<Specializations> specList)
    {
        Specializations newSpec = game.offerChangeSpec(this, specList);
        if (newSpec != null)//пользователь выбрал новую специализацию
            bacterium.SetSpecialization(SpecializationFactory.create(newSpec));
    }
}
