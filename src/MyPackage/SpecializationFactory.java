/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

import java.awt.Color;

/**
 *
 * @author Elena Sarkisova
 */
public class SpecializationFactory {
    static public Specialization create(Specializations s)
    {
        Specialization newSpec = null;
        switch(s){
            case SIMPLEST_ANIMAL:
                newSpec = new Specialization(new Color(204,102,0), 
                        Specializations.SIMPLEST_ANIMAL,
                        Ration.getSimplestAnimalRation(),
                        "SIMP.A");
                break;
            case SIMPLEST_PLANT:
                newSpec = new Specialization(Color.GREEN, 
                        Specializations.SIMPLEST_PLANT,
                        Ration.getSimplestPlantRation(),
                        "SIMP.P");
                break;
            case PRIMARY_BACTERIUM:
                newSpec = new Specialization(new Color(153,255,255), 
                        Specializations.PRIMARY_BACTERIUM,
                        Ration.getPrimaryBacteriumRation(),
                        "BACT");
                break;
            case MOSS_BACTERIUM:
                newSpec = new Specialization(new Color(51,25,0), 
                        Specializations.MOSS_BACTERIUM,
                        Ration.getMossBacteriumRation(),
                        "MOSS");
                break;
            case PLANT_PARASITE:
                newSpec = new Specialization(new Color(102,102,0), 
                        Specializations.PLANT_PARASITE,
                        Ration.getPlantParasiteRation(),
                        "PARASITE.P");
                break;
            case PLANT_PREDATOR:
                newSpec = new Specialization(new Color(153,0,0), 
                        Specializations.PLANT_PREDATOR,
                        Ration.getPlantPredatorRation(),
                        "PREDATOR.P");
                break;
            case HERBIVOROUS_ANIMAL:
                newSpec = new Specialization(new Color(0,204,0), 
                        Specializations.HERBIVOROUS_ANIMAL,
                        Ration.getHerbivorousAnimalRation(),
                        "HERB.A");
                break;
            case CARNIVOROUS_ANIMAL:
                newSpec = new Specialization(new Color(255,102,178), 
                        Specializations.CARNIVOROUS_ANIMAL,
                        Ration.getCarnivorousAnimalRation(),
                        "CARN.A");
                break;
            case BUFFALO_BACTERIUM:
                newSpec = new Specialization(new Color(204,0,204), 
                        Specializations.BUFFALO_BACTERIUM,
                        Ration.getBuffaloBacteriumRation(),
                        "BUFF");
                break;
            case OMNIVORE_ANIMAL:
                newSpec = new Specialization(new Color(153,51,255), 
                        Specializations.OMNIVORE_ANIMAL,
                        Ration.getOmnivoreAnimalRation(),
                        "OMNI.A");
                break;
            case TIGER_BACTERIUM:
                newSpec = new Specialization(new Color(255,128,0), 
                        Specializations.TIGER_BACTERIUM,
                        Ration.getTigerBacteriumRation(),
                        "TIGER");
                break;
            default:
                throw new AssertionError(s.name());
        }
        return newSpec;
    }
}