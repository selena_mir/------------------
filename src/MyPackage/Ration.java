/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

import java.util.ArrayList;

/**
 * Рецепты
 * @author Elena Sarkisova
 */
public class Ration {
    ArrayList <Recipe> recipes = new ArrayList<Recipe>();
    public void add(Recipe r)
    {
        recipes.add(r);
    }
    public boolean check(BaseElement element, int weight)
    {
        for(Recipe r : recipes)
        {
            if(r.check(element, weight))
                return true;
        }
        return false;
    }
    public  Recipe getPosibleRecipe(ArrayList <BaseElement> food, ArrayList<BaseElement> col){
        for (Recipe r : recipes){
            if (r.isPosible(food, col))
                return r;
        }
        return null;
    } 
    //рацион первичной бактерии
    public static Ration getPrimaryBacteriumRation(){
        Ration r = new Ration();
        
        Recipe rep1 = new Recipe();
        rep1.add(new Agar());
        rep1.add(new Light());
        rep1.add(new Water());
        rep1.setEfficiency(1);
        rep1.setResult(NutritiousParticles.CO2);
        r.add(rep1);
        
        rep1 = new Recipe();
        rep1.add(Specializations.PRIMARY_BACTERIUM, 1);
        rep1.setEfficiency(1);
        rep1.setResult(NutritiousParticles.CO2);
        r.add(rep1);
        
        return r;
    }
    //рацион простейшего животного
    public static Ration getSimplestAnimalRation(){
        Ration r = new Ration();
        
        Recipe rep = new Recipe();
        rep.add(new Oxygen());
        rep.add(new Water());
        rep.add(Specializations.PRIMARY_BACTERIUM, 1.2);
        rep.setEfficiency(0.9);
        rep.setResult(NutritiousParticles.CO2);
        r.add(rep);
        
        return r;
    }
    //рацион простейшего растения
    public static Ration getSimplestPlantRation(){
        Ration r = new Ration();
        
        Recipe rep = new Recipe();
        rep.add(new CO2());
        rep.add(new Light());
        rep.add(new Water());
        rep.setEfficiency(0.9);
        rep.setResult(NutritiousParticles.OXYGEN);
        r.add(rep);
        
        rep = new Recipe();
        rep.add(Specializations.PRIMARY_BACTERIUM, 1);
        rep.setEfficiency(0.9);
        rep.setResult(NutritiousParticles.OXYGEN);
        r.add(rep);
        
        return r;
    }
    //рацион бактерии-мха
    public static Ration getMossBacteriumRation(){
        Ration r = new Ration();
        
        Recipe rep = new Recipe();
        rep.add(new CO2());
        rep.add(new Light());
        rep.add(new Water());
        rep.setEfficiency(1.0);
        rep.setResult(NutritiousParticles.OXYGEN);
        r.add(rep);
        
        return r;
    }
    //рацион растения-паразита
    public static Ration getPlantParasiteRation(){
        Ration r = new Ration();
        
        Recipe rep = new Recipe();
        rep.add(new CO2());
        rep.add(new Light());
        rep.add(new Water());
        rep.setEfficiency(0.8);
        rep.setResult(NutritiousParticles.OXYGEN);
        r.add(rep);
        
        ArrayList<Specializations> plants = EvolutionTree.getAllDescendans(Specializations.SIMPLEST_PLANT);//список всех растений
        for(int i = 0 ; i < plants.size(); i++)
        {
            rep = new Recipe();
            rep.add(plants.get(i),1.5);
            rep.setEfficiency(1.0);
            rep.setResult(NutritiousParticles.OXYGEN);
            r.add(rep);
        }
        
        return r;
    }
    //рацион растения - хищника
    public static Ration getPlantPredatorRation(){
        Ration r = new Ration();
        
        Recipe rep = new Recipe();
        rep.add(new CO2());
        rep.add(new Light());
        rep.add(new Water());
        rep.setEfficiency(0.9);
        rep.setResult(NutritiousParticles.OXYGEN);
        r.add(rep);
        
        ArrayList<Specializations> animals = EvolutionTree.getAllDescendans(Specializations.SIMPLEST_ANIMAL);//список всех животных
        for(int i = 0 ; i < animals.size();i++)
        {
            rep = new Recipe();
            rep.add(animals.get(i),0.5);
            rep.setEfficiency(1.0);
            rep.setResult(NutritiousParticles.OXYGEN);
            r.add(rep);
        }
        
        return r;
    }
    //рацион растениядного животного
    public static Ration getHerbivorousAnimalRation(){
        Ration r = new Ration();
        
        ArrayList<Specializations> plants = EvolutionTree.getAllDescendans(Specializations.SIMPLEST_PLANT);//список всех животных
        for(int i = 0 ; i < plants.size();i++)
        {
            Recipe rep = new Recipe();
            rep.add(new Oxygen());
            rep.add(new Water());
            rep.add(plants.get(i),1.25);
            rep.setEfficiency(0.9);
            rep.setResult(NutritiousParticles.CO2);
            r.add(rep);
        }
        
        return r;
    }
    //рацион бактерии-буйвола
    public static Ration getBuffaloBacteriumRation(){
        Ration r = new Ration();
        
        ArrayList<Specializations> plants = EvolutionTree.getAllDescendans(Specializations.SIMPLEST_PLANT);//список всех растений
        for(int i = 0 ; i < plants.size();i++)
        {
            Recipe rep = new Recipe();
            rep.add(new Oxygen());
            rep.add(new Water());
            rep.add(plants.get(i),1.25);
            rep.setEfficiency(0.8);
            rep.setResult(NutritiousParticles.CO2);
            r.add(rep);
        }
        
        Recipe rep = new Recipe();
        rep.add(new Oxygen());
        rep.add(new Water());
        rep.add(Specializations.MOSS_BACTERIUM,1.5);
        rep.setEfficiency(1);
        rep.setResult(NutritiousParticles.CO2);
        r.add(rep);
        
        return r;
    }
    //рацион хищного животного
    public static Ration getCarnivorousAnimalRation(){
        Ration r = new Ration();
        
        Recipe rep = new Recipe();
        rep.add(new Oxygen());
        rep.add(new Water());
        rep.add(Specializations.SIMPLEST_ANIMAL,2);
        rep.setEfficiency(0.9);
        rep.setResult(NutritiousParticles.CO2);
        r.add(rep);
        
        Recipe rep1 = new Recipe();
        rep1.add(new Oxygen());
        rep1.add(new Water());
        rep1.add(Specializations.HERBIVOROUS_ANIMAL,2);
        rep1.setEfficiency(1);
        rep1.setResult(NutritiousParticles.CO2);
        r.add(rep1);
        
        return r;
    }
    //рацион бактерии-тигра
    public static Ration getTigerBacteriumRation(){
        Ration r = new Ration();
        
        Recipe rep = new Recipe();
        rep.add(new Oxygen());
        rep.add(new Water());
        rep.add(Specializations.SIMPLEST_ANIMAL,2);
        rep.setEfficiency(0.9);
        rep.setResult(NutritiousParticles.CO2);
        r.add(rep);
        
        Recipe rep1 = new Recipe();
        rep1.add(new Oxygen());
        rep1.add(new Water());
        rep1.add(Specializations.HERBIVOROUS_ANIMAL,2);
        rep1.setEfficiency(0.8);
        rep1.setResult(NutritiousParticles.CO2);
        r.add(rep1);
        
        Recipe rep2 = new Recipe();
        rep2.add(new Oxygen());
        rep2.add(new Water());
        rep2.add(Specializations.BUFFALO_BACTERIUM,1.5);
        rep2.setEfficiency(1);
        rep2.setResult(NutritiousParticles.CO2);
        r.add(rep2);
        
        return r;
    }
    //рацион всеядного животного
    public static Ration getOmnivoreAnimalRation(){
        Ration r = new Ration();
        
        ArrayList<Specializations> plants = EvolutionTree.getAllDescendans(Specializations.SIMPLEST_PLANT);//список всех растений
        for(int i = 0 ; i < plants.size();i++)
        {
            Recipe rep = new Recipe();
            rep.add(new Oxygen());
            rep.add(new Water());
            rep.add(plants.get(i),1.25);
            rep.setEfficiency(0.8);
            rep.setResult(NutritiousParticles.CO2);
            r.add(rep);
        }
        
        ArrayList<Specializations> animals = EvolutionTree.getAllDescendans(Specializations.SIMPLEST_ANIMAL);//список всех животных
        for(int i = 0 ; i < animals.size();i++)
        {
            Recipe rep = new Recipe();
            rep.add(new Oxygen());
            rep.add(new Water());
            rep.add(animals.get(i),2);
            rep.setEfficiency(0.8);
            rep.setResult(NutritiousParticles.CO2);
            r.add(rep);
        }
        
        return r;
    }
}