/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

/**
 * Питательные частицы
 * @author Elena Sarkisova
 */
public enum NutritiousParticles {
    LIGHT,
    AGAR,
    CO2,
    OXYGEN,
    WATER
}
