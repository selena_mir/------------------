/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author MegoComp
 */
public class PlayerBot extends Player{
    public PlayerBot(Bacterium bacterium, MyGame game) {
        super(bacterium, game);
    }
    
    @Override
    public void move(){
        Point p = findTarget();
        moveTo(p.x, p.y);
    }
    
    private Point findTarget(){
        ArrayList<BaseElement> nutritiousParticles = game.getNutritiousParticles();
        ArrayList<BaseElement> entitys = game.getEntitys();
        ArrayList<BaseElement> enemies = new ArrayList<>();
        for (BaseElement entity : entitys){
            if (entity != bacterium && ((Entity)entity).specialization.ration.check(bacterium, entity.weight)){
                if (pointInArea(new Point((int)entity.centerX(), (int)entity.centerY()), 4 * bacterium.R))
                enemies.add(entity);
            }
        }
        if (enemies.isEmpty()){
            ArrayList<BaseElement> bes = new ArrayList<>();
            for(BaseElement np : nutritiousParticles){
                if (bacterium.specialization.ration.check(np, bacterium.weight))
                    bes.add(np);
            }
            for(BaseElement e : entitys){
                if (e != bacterium && bacterium.specialization.ration.check(e, bacterium.weight))
                    bes.add(e);
            }

            if (bes.isEmpty())
                return new Point((int)bacterium.centerX(), (int)bacterium.centerY());
            BaseElement e = bes.get(0);
            double d = Math.sqrt(e.getX() * e.getX() + e.getY() * e.getY());
            for(BaseElement np : bes){
                double id = Math.sqrt(np.getX() * np.getX() + np.getY() * np.getY());
                if (id < d){
                    d = id;
                    e = np;
                }
            }
            return new Point((int)e.getX(), (int)e.getY());
        }
        
        return pointToEscapeArea(new Point((int)enemies.get(0).centerX(), (int)enemies.get(0).centerY()), 4 * bacterium.R);
    }
    
    private Point pointToEscapeArea(Point center, double r){
        double d = 10;
        for (double i=bacterium.R + d; i<2000;i+=d){
            Point p = new Point((int) (bacterium.centerX()+i), (int)bacterium.centerY());
            if (!deltaBetwinPoints(center, p, r))
                return p;
            p=new Point((int) (bacterium.centerX()), (int)(bacterium.centerY()+i));
            if (p.x >0 && p.y>0 && p.x<game.getWidth() && p.y<game.getHeight() && !deltaBetwinPoints(center, p, r))
                return p;
            p=new Point((int) (bacterium.centerX()-i), (int)bacterium.centerY());
            if (p.x >0 && p.y>0 && p.x<game.getWidth() && p.y<game.getHeight() && !deltaBetwinPoints(center, p, r))
                return p;
            p=new Point((int) (bacterium.centerX()), (int)(bacterium.centerY()-i));
            if (p.x >0 && p.y>0 && p.x<game.getWidth() && p.y<game.getHeight() && !deltaBetwinPoints(center, p, r))
                return p;
            
            p=new Point((int) (bacterium.centerX()+Math.sqrt(i*i+i*i)), (int)(bacterium.centerY()+Math.sqrt(i*i+i*i)));
            if (p.x >0 && p.y>0 && p.x<game.getWidth() && p.y<game.getHeight() && !deltaBetwinPoints(center, p, r))
                return p;
            p=new Point((int) (bacterium.centerX()+Math.sqrt(i*i+i*i)), (int)(bacterium.centerY()-Math.sqrt(i*i+i*i)));
            if (p.x >0 && p.y>0 && p.x<game.getWidth() && p.y<game.getHeight() && !deltaBetwinPoints(center, p, r))
                return p;
            p=new Point((int) (bacterium.centerX()-Math.sqrt(i*i+i*i)), (int)(bacterium.centerY()+Math.sqrt(i*i+i*i)));
            if (p.x >0 && p.y>0 && p.x<game.getWidth() && p.y<game.getHeight() && !deltaBetwinPoints(center, p, r))
                return p;
            p=new Point((int) (bacterium.centerX()-Math.sqrt(i*i+i*i)), (int)(bacterium.centerY()-Math.sqrt(i*i+i*i)));
            if (p.x >0 && p.y>0 && p.x<game.getWidth() && p.y<game.getHeight() && !deltaBetwinPoints(center, p, r))
                return p;
        }
        return center;
    }
    private boolean deltaBetwinPoints(Point p1, Point p2, double delta){
        return Math.sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y)) <=delta;
    }
    /*
    выбрать специализацию
    */
    public void chooseSpec(ArrayList<Specializations> specList)
    {
        Random random = new Random();
        boolean changeSpec = random.nextBoolean();
        if (!changeSpec) return;
        int i = random.nextInt() % specList.size();
        i = i > 0 ? i : -i;
        Specializations spec = specList.get(i);
        bacterium.SetSpecialization(SpecializationFactory.create(spec));
    }
}
