/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyPackage;

import java.util.EventObject;

/**
 *
 * @author Elena Sarkisova
 */
public class EntityEvent extends EventObject{

    private int weightDelta = 0;
    private Entity entity = null;
    private NutritiousParticle result = null;
    public EntityEvent(Object source) {
        super(source);
    }
    
    EntityEvent(Entity entity, int weightDelta,NutritiousParticle res) {
        super(entity);
        this.entity = entity;
        this.weightDelta = weightDelta;
        this.result = res;
    }
    public int getWeightDelta()
    {
        return weightDelta;
    }
    public Entity getEntity()
    {
        return entity;
    }
    public NutritiousParticle getResult()
    {
        return result;
    }
}
